package sd.a4.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import sd.a4.dao.PackageDAOImpl;
import sd.a4.dao.UserDAO;
import sd.a4.dao.UserDAOImpl;
import sd.a4.model.Package;
import sd.a4.model.User;

@WebService(endpointInterface = "sd.a4.service.UserService") 
public class UserServiceImpl implements UserService {

	UserDAOImpl dao = new UserDAOImpl();
	PackageDAOImpl pDao = new PackageDAOImpl();
	
	public void addUser(User u) throws SQLException {
		dao.addUser(u);
	}

	public User getUser(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public User login(String name, String password) throws SQLException {
		return dao.login(name, password);
	}

	public String[] packageList(String user) throws SQLException {
		return pDao.packageList(user);
	}

	public String[] locationList(String pack) throws SQLException {
		return pDao.locationList(pack);
	}

	//public List<User> userList() {
		// TODO Auto-generated method stub
	//	return null;
//	}

}
