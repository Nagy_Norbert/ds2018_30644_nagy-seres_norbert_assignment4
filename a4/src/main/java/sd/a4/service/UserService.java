package sd.a4.service;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import sd.a4.model.*;
import sd.a4.model.Package;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {

	@WebMethod
	public void addUser(User u) throws SQLException;
	
	@WebMethod
	public User login(String name, String password) throws SQLException;
	
	@WebMethod
	public User getUser(int id);
	
	@WebMethod
	public String[] packageList(String user) throws SQLException;
	
	@WebMethod
	public String[] locationList(String pack) throws SQLException;
	
//	@WebMethod
//	public List<User> userList();
}
