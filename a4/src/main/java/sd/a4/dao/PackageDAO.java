package sd.a4.dao;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sd.a4.model.Package;

public interface PackageDAO {

	void addPackage(Package p) throws SQLException;
	String[] packageList(String user) throws SQLException;
	String[] locationList(String pack) throws SQLException;
	
}
