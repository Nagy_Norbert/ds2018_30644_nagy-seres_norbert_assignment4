package sd.a4.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.*;


import sd.a4.connection.ConnectionFactory;
import sd.a4.model.User;

public class UserDAOImpl implements UserDAO{
	private static final String insertStatementString = "INSERT INTO `user` VALUES (?,?,?,?)";


	public void addUser(User u) throws SQLException {

		Connection dbConnection = (Connection) ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
		insertStatement.setInt(1, u.getId());
		insertStatement.setString(2, u.getName());
		insertStatement.setString(3, "USER");
		insertStatement.setString(4, u.getPassword());
		insertStatement.executeUpdate();

		ConnectionFactory.close(insertStatement);
		ConnectionFactory.close(dbConnection);
	}

	private static final String loginString = "SELECT * FROM `user` WHERE name=?";

	public User login(String name, String password) throws SQLException {

		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement(loginString, Statement.RETURN_GENERATED_KEYS);
		insertStatement.setString(1, name);
	//	insertStatement.setString(2, password);
		rs=insertStatement.executeQuery();
		if(rs.next()) {

		
		
		User aux = new User();
		aux.setId(rs.getInt("id"));
		aux.setName(rs.getString("name"));
		aux.setRole(rs.getString("role"));
		aux.setPassword(rs.getString("password"));
		
		System.out.println(aux.getId() + aux.getName() + aux.getPassword());
		rs.close();
		ConnectionFactory.close(insertStatement);
		ConnectionFactory.close(dbConnection);
		return aux;
		}
		User aux = new User();
		return aux;
	}
}
