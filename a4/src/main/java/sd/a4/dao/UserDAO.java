package sd.a4.dao;

import java.sql.SQLException;

import sd.a4.model.User;

public interface UserDAO {
	
	void addUser(User u) throws SQLException;
	User login(String name, String password) throws SQLException;
	
}
