package sd.a4.presentation;



import java.awt.*;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import sd.a4.model.User;
import sd.a4.service.UserService;
import sd.a4.model.Package;

public class ViewUser extends JFrame  {

	private static final long serialVersionUID = 1L;
	
	private JTextField username=new JTextField(20);
	private JTextField password=new JTextField(20);

	private JButton button=new JButton("Status check");
	private JTextArea textArea = new JTextArea();
	
	JFrame frame;
	JPanel panel1, panel2, panel3;

	private String lists[];
	JComboBox list = new JComboBox();

	public ViewUser(User user) {
		
		URL wsdlURL;
		try {
			wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
		

		QName qname = new QName("http://service.a4.sd/", "UserServiceImplService"); 
		
		Service service = Service.create(wsdlURL, qname);  

		UserService ps = service.getPort(UserService.class);
		

		
		list = new JComboBox(ps.packageList(user.getName()));
		
	//	User p1 = ps.login(v.getName(), v.getPAssword());

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 frame = new JFrame ("User");
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setSize(600, 400);

		 panel1 = new JPanel();
		 panel2 = new JPanel();
		 panel3 = new JPanel();


		 JLabel l_username = new JLabel ("Your packages: ");
		 
		 panel1.add(l_username);
		 panel2.add(list);
		 panel2.add(button);
		 panel3.add(textArea);
		 
		 JPanel p = new JPanel();
		 p.add(panel1);
		 p.add(panel2);
		 p.add(panel3);


		 p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		 frame.setContentPane(p);
		 frame.setVisible(true);
		 
		}

	

	public void buttonListener(ActionListener e) {
        button.addActionListener(e);
    }

	
	public String getSelected() {
		return  (String) list.getSelectedItem();
	}

	public void setText(String[] message) {
		String aux="";
		for(String s: message) {
			aux += s +"\n";
		}
		textArea.setText(aux);
	}
	
}


