package sd.a4.presentation;



import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import sd.a4.model.User;
import sd.a4.service.UserService;

import java.awt.event.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

public class ControllerUser {
	

	
	private ViewUser v;
	
	public ControllerUser(ViewUser v){
		this.v=v;
		
		v.buttonListener(new buttonListener());

	}
	
	class buttonListener implements ActionListener {

		public void actionPerformed(ActionEvent e)
		{
			
			URL wsdlURL;
			try {
				wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
			

			QName qname = new QName("http://service.a4.sd/", "UserServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  

			UserService ps = service.getPort(UserService.class);
			
			v.setText(ps.locationList(v.getSelected()));
			//v.newFrame(p1);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			
		
	}
	
	
	
}
	


