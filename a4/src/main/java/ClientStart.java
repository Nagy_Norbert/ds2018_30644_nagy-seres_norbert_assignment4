

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import sd.a4.model.User;
import sd.a4.presentation.Controller;
import sd.a4.presentation.View;
import sd.a4.service.UserService;
import sd.a4.model.Package;

public class ClientStart {

	public static void main(String[] args) throws MalformedURLException, SQLException {
		URL wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
		//check above URL in browser, you should see WSDL file
		
		//creating QName using targetNamespace and name
		QName qname = new QName("http://service.a4.sd/", "UserServiceImplService"); 
		
		Service service = Service.create(wsdlURL, qname);  
		
		//We need to pass interface and model beans to client
		UserService ps = service.getPort(UserService.class);
		
		User p1 = new User();
		p1.setName("user1");
		p1.setPassword("user1");

		User p2 = ps.login(p1.getName(), p1.getPassword());
		//System.out.println(p2.getName());
		String[] list = ps.packageList("user1");
		System.out.println(list[0]);
		View v = new View();
		Controller c = new Controller(v);
		
		
	}

}
