package sd.a4.admin.presentation;



import java.awt.*;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import javax.swing.*;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;



public class View extends JFrame  {

	private static final long serialVersionUID = 1L;
	
	private JTextField sender=new JTextField(20);
	private JTextField receiver=new JTextField(20);
	private JTextField name=new JTextField(20);
	private JTextField description=new JTextField(20);
	private JTextField sender_city=new JTextField(20);
	private JTextField destination_city=new JTextField(20);

	private JTextField pack=new JTextField(20);
	private JTextField loc_and_time=new JTextField(20);
	
	private JButton button=new JButton("Add");
	private JButton button2 = new JButton("Add entry");
	private JTextArea textArea = new JTextArea();
	
	JFrame frame;
	JPanel panel1, panel2, panel3;
	JFrame frame2;


	public View() {
		
		
		
		 frame = new JFrame ("Cars");
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setSize(600, 400);

		 panel1 = new JPanel();
		 panel2 = new JPanel();
		 panel3 = new JPanel();


		 JLabel l_sender = new JLabel ("Sender: ");
		 JLabel l_receiver = new JLabel ("Receiver: ");
		 JLabel l_name = new JLabel("Name: ");
		 JLabel l_description = new JLabel ("Description: ");
		 JLabel l_sender_city = new JLabel ("Sender city: ");
		 JLabel l_destination_city = new JLabel("Destination city: ");
		 

		 
		 panel1.add(l_sender);
		 panel1.add(sender);
		 panel1.add(l_receiver);
		 panel1.add(receiver);
		 panel1.add(l_name);
		 panel1.add(name);
		 panel1.add(l_description);
		 panel1.add(description);
		 panel1.add(l_sender_city);
		 panel1.add(sender_city);
		 panel1.add(l_destination_city);
		 panel1.add(destination_city);
		 
		 JLabel pack_l = new JLabel("Package name:");
		 JLabel loc = new JLabel("Location and time:"); 
		 
		 
		 panel2.add(button);
		 
		 panel2.add(textArea);
		 
		 panel3.add(pack_l);
		 
		 panel3.add(pack);
		 panel3.add(loc);
		 panel3.add(loc_and_time);
		 panel3.add(button2);
		 
		 JPanel p = new JPanel();
		 p.add(panel1);
		 p.add(panel2);
		 p.add(panel3);


		 p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		 frame.setContentPane(p);
		 frame.setVisible(true);
		 
		 
	
	}
	
	public String getSender(){
		return sender.getText();
	}
	
	public String getReceiver(){
		return receiver.getText();
	}
	
	public String getName(){
		return name.getText();
	}
	
	public String getDescription(){
		return description.getText();
	}
	
	public String getSenderCity(){
		return sender_city.getText();
	}
	
	public String getDestinationCity(){
		return destination_city.getText();
	}
	
	public String getPackage() {
		return pack.getText();
	}
	
	public String getLocationAndTime() {
		return loc_and_time.getText();
	}

	public void buttonListener(ActionListener e) {
        button.addActionListener(e);
    }

	public void button2Listener(ActionListener e) {
		button2.addActionListener(e);
	}
	
}


