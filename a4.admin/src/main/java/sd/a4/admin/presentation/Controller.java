package sd.a4.admin.presentation;



import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import sd.a4.admin.service.AdminService;

import java.awt.event.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import sd.a4.admin.model.Package;

public class Controller {
	

	
	private View v;
	
	public Controller(View v){
		this.v=v;
		
		v.buttonListener(new buttonListener());
		v.button2Listener(new button2Listener());

	}
	
	class buttonListener implements ActionListener {
					
		public void actionPerformed(ActionEvent e)
		{
			try {
			URL wsdlURL = new URL("http://localhost:8889/ws/admin?wsdl");
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.admin.a4.sd/", "AdminServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			AdminService as = service.getPort(AdminService.class);
			
			Package p = new Package();
			p.setSender(v.getSender());
			p.setReceiver(v.getReceiver());
			p.setName(v.getName());
			p.setDescription(v.getDescription());
			p.setSender_city(v.getSenderCity());
			p.setDestination_city(v.getDestinationCity());
			as.addPackage(p);
			//v.newFrame(p1);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	class button2Listener implements ActionListener {
		
		public void actionPerformed(ActionEvent e)
		{
			try {
			URL wsdlURL = new URL("http://localhost:8889/ws/admin?wsdl");
			//check above URL in browser, you should see WSDL file
			
			//creating QName using targetNamespace and name
			QName qname = new QName("http://service.admin.a4.sd/", "AdminServiceImplService"); 
			
			Service service = Service.create(wsdlURL, qname);  
			
			//We need to pass interface and model beans to client
			AdminService as = service.getPort(AdminService.class);
			
			as.addTracking(v.getPackage(), v.getLocationAndTime());
			//v.newFrame(p1);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	
	
}
	


