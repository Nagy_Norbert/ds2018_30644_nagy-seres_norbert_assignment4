package sd.a4.admin;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import sd.a4.admin.service.AdminService;
import sd.a4.admin.model.Package;
import sd.a4.admin.presentation.Controller;
import sd.a4.admin.presentation.View;

public class StartAdmin {

	public static void main(String[] args) throws MalformedURLException, SQLException {
		
		View v = new View();
		Controller c = new Controller(v);
}
}
