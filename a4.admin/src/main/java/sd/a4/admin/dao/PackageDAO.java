package sd.a4.admin.dao;

import java.sql.SQLException;
import java.util.List;

import sd.a4.admin.model.Package;

public interface PackageDAO {

	void addPackage(Package p) throws SQLException;
	List<Package> packageList(String user) throws SQLException;
	void addTracking(String pack, String loc_and_time) throws SQLException;
	
}
