package sd.a4.admin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import sd.a4.admin.model.Package;
import sd.a4.admin.connection.ConnectionFactory;


import java.sql.SQLException;

public class PackageDAOImpl implements PackageDAO {

private static final String insertStatementString = "INSERT INTO `package` VALUES (?,?,?,?,?,?,?,?)";
	
	public void addPackage(Package p) throws SQLException {

	Connection dbConnection = (Connection) ConnectionFactory.getConnection();

	PreparedStatement insertStatement = null;
		insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
		insertStatement.setInt(1, p.getId());
		insertStatement.setString(2, p.getSender());
		insertStatement.setString(3, p.getReceiver());
		insertStatement.setString(4, p.getName());
		insertStatement.setString(5, p.getDescription());
		insertStatement.setString(6, p.getSender_city());
		insertStatement.setString(7, p.getDestination_city());
		insertStatement.setBoolean(8, p.isTracking());
		insertStatement.executeUpdate();

		ConnectionFactory.close(insertStatement);
		ConnectionFactory.close(dbConnection);
	}

	private static final String selectString = "SELECT * FROM `package` WHERE receiver=?";
	
	public List<Package> packageList(String user) throws SQLException {
		List<Package> aux = new ArrayList<Package>();
		Connection dbConnection = (Connection) ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		selectStatement = (PreparedStatement) dbConnection.prepareStatement(selectString, Statement.RETURN_GENERATED_KEYS);
		selectStatement.setString(1, user);
		rs=selectStatement.executeQuery();
		while(rs.next()) {
			Package p = new Package();
			p.setId(rs.getInt("idpackage"));
			p.setSender(rs.getString("sender"));
			p.setReceiver(rs.getString("receiver"));
			p.setName(rs.getString("name"));
			p.setDescription(rs.getString("description"));
			p.setSender_city(rs.getString("sender_city"));
			p.setDestination_city(rs.getString("destination_city"));
			p.setTracking(rs.getBoolean("tracking"));
			aux.add(p);
		}
		return aux;
	}

	private static final String insertString = "INSERT INTO `tracking`(package,location_and_time) VALUES (?,?)";
	
	public void addTracking(String pack, String loc_and_time) throws SQLException {
		Connection dbConnection = (Connection) ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
			insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, pack);
			insertStatement.setString(2, loc_and_time);

			insertStatement.executeUpdate();

			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}

}
