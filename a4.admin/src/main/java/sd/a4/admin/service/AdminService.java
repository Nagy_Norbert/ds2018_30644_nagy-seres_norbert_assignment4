package sd.a4.admin.service;



import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


import sd.a4.admin.model.Package;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AdminService {

	@WebMethod
	public void addPackage(Package p) throws SQLException;
	
	@WebMethod
	public void addTracking(String pack, String locAndTime) throws SQLException;
	
	
}
